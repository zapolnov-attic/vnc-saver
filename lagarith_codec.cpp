//
//	Wrapper for the Lagarith codec.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "lagarith_codec.h"
#include <QApplication>

LagarithCodec::LagarithCodec(QObject * parent)
	: Codec(parent),
	  m_BufferSize(0),
	  m_OutputFormat(NULL),
	  m_FrameNum(0),
	  m_FrameDataSize(0)
{
	m_CodecHandle = (DWORD_PTR)DriverProc(0, NULL, DRV_OPEN, 0, 0);
	if (!m_CodecHandle)
		qDebug("Lagarith: not enough memory.");
}

LagarithCodec::~LagarithCodec()
{
	if (m_CodecHandle)
		DriverProc(m_CodecHandle, NULL, DRV_CLOSE, 0, 0);

	if (m_OutputFormat)
		free(m_OutputFormat);
}

bool LagarithCodec::init(int width, int height)
{
	if (!m_CodecHandle)
		return false;

	m_FrameNum = 0;
	m_FrameDataSize = width * height;

	m_InputFormat.biSize = sizeof(BITMAPINFOHEADER);
	m_InputFormat.biWidth = width;
	m_InputFormat.biHeight = height;
	m_InputFormat.biPlanes = 1;
	m_InputFormat.biBitCount = 32;
	m_InputFormat.biCompression = 0;
	m_InputFormat.biSizeImage = 0;
	m_InputFormat.biXPelsPerMeter = 0;
	m_InputFormat.biYPelsPerMeter = 0;
	m_InputFormat.biClrUsed = 0;
	m_InputFormat.biClrImportant = 0;

	LRESULT lFormatSize = DriverProc(m_CodecHandle, NULL, ICM_COMPRESS_GET_FORMAT, (LPARAM)&m_InputFormat, 0);
	if (lFormatSize <= 0)
	{
		qDebug("Lagarith: unable to determine size of the output format.");
		return false;
	}

	LPBITMAPINFOHEADER outputFormat = (LPBITMAPINFOHEADER)realloc(m_OutputFormat, (size_t)lFormatSize);
	if (!outputFormat)
	{
		qDebug("Lagarith: not enough memory.");
		return false;
	}
	m_OutputFormat = outputFormat;

	if (DriverProc(m_CodecHandle, NULL, ICM_COMPRESS_GET_FORMAT, (LPARAM)&m_InputFormat,
		(LPARAM)m_OutputFormat) < 0)
	{
		qDebug("Lagarith: unable to initialize output format.");
		return false;
	}

	m_BufferSize = (int)DriverProc(m_CodecHandle, NULL, ICM_COMPRESS_GET_SIZE, (LPARAM)&m_InputFormat,
		(LPARAM)m_OutputFormat);
	if (m_BufferSize <= 0)
	{
		qDebug("Lagarith: unable to determine size of the compression buffer.");
		return false;
	}
	m_Buffer.resize(m_BufferSize);

	if (DriverProc(m_CodecHandle, NULL, ICM_COMPRESS_BEGIN, (LPARAM)&m_InputFormat, (LPARAM)m_OutputFormat)
		!= ICERR_OK)
	{
		qDebug("Lagarith: unable to start the compression.");
		return false;
	}

	return true;
}

bool LagarithCodec::end()
{
	if (!m_CodecHandle)
		return false;

	if (DriverProc(m_CodecHandle, NULL, ICM_COMPRESS_END, 0, 0) != ICERR_OK)
	{
		qDebug("Lagarith: unable to finish the compression.");
		return false;
	}

	return true;
}

QByteArray LagarithCodec::compressFrame(const void * frameData)
{
	DWORD dwFlags = 0;
	ICCOMPRESS c;

	c.dwFlags = 0;
	c.lpbiOutput = m_OutputFormat;
	c.lpOutput = m_Buffer.data();
	c.lpbiInput = &m_InputFormat;
	c.lpInput = (LPVOID)frameData;
	c.lpckid = NULL;
	c.lpdwFlags = &dwFlags;
	c.lFrameNum = m_FrameNum;
	c.dwFrameSize = 0;
	c.dwQuality = 1000;
	c.lpbiPrev = (m_FrameNum == 0 ? NULL : &m_InputFormat);
	c.lpPrev = (m_FrameNum == 0 ? NULL : m_PrevFrameData.data());

	if (DriverProc(m_CodecHandle, NULL, ICM_COMPRESS, (LPARAM)&c, (LPARAM)sizeof(c)) != ICERR_OK)
	{
		qDebug("Lagarith: unable to compress frame %ld.", (long)m_FrameNum);
		return QByteArray();
	}

	++m_FrameNum;
	m_PrevFrameData = QByteArray((const char *)frameData, m_FrameDataSize);

	return m_Buffer.left(m_OutputFormat->biSizeImage);
}
