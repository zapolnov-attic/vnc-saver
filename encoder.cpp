//
//	Video stream encoder class.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "encoder.h"
#include <QApplication>
#include <QPainter>

Encoder::Encoder(Codec * codec, QIODevice * output, QObject * parent)
	: QObject(parent),
	  m_Codec(codec),
	  m_Output(output),
	  m_Image(NULL),
	  m_InFrame(false)
{
}

Encoder::~Encoder()
{
	if (m_InFrame)
		endFrame();

	m_Codec->end();
	m_Output->close();

	delete m_Image;
}

void Encoder::beginFrame(int w, int h)
{
	if (!m_Image)
	{
		if (!m_Codec->init(w, h))
			return;

		m_Image = new QImage(w, h, QImage::Format_ARGB32);
		m_Image->fill(Qt::black);
	}

	m_InFrame = true;
}

void Encoder::processRect(int x, int y, int w, int h, QByteArray data)
{
	if (!m_Image)
		return;

	QImage image((unsigned char *)data.data(), w, h, QImage::Format_ARGB32);
	QPainter painter(m_Image);
	painter.drawImage(x, y, image);
	painter.end();
}

void Encoder::endFrame()
{
	m_InFrame = false;
	if (!m_Image)
		return;

	QByteArray data = m_Codec->compressFrame(m_Image->constBits());
	m_Output->write(data);
}
