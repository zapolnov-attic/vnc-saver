//
//	Very simple VNC client class.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "vnc_client.h"

VNCClient::VNCClient(const QString & host, int port, QObject * parent)
	: QObject(parent)
{
	m_Socket = new QTcpSocket(this);
	connect(m_Socket, SIGNAL(connected()), SLOT(connected()));
	connect(m_Socket, SIGNAL(disconnected()), SLOT(disconnected()));
	connect(m_Socket, SIGNAL(readyRead()), SLOT(readyRead()));
	qDebug("VNC: connecting to %s:%d", qPrintable(host), port);
	m_State = HandshakeBegin;
	m_Socket->connectToHost(host, port);
}

void VNCClient::disconnect()
{
	if (m_Socket)
	{
		qDebug("VNC: closing the connection.");
		QObject::disconnect(m_Socket, SIGNAL(connected()), this, SLOT(connected()));
		QObject::disconnect(m_Socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
		QObject::disconnect(m_Socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
		m_Socket->deleteLater();
		m_Socket = NULL;
		emit connectionClosed();
	}
}

void VNCClient::connected()
{
	qDebug("VNC: connection established.");
	m_State = HandshakeBegin;
}

void VNCClient::disconnected()
{
	qDebug("VNC: connection has been closed.");
	m_Socket->deleteLater();
	m_Socket = NULL;
	emit connectionClosed();
}

void VNCClient::readyRead()
{
  restart:
	if (!m_Socket)
		return;

	switch (m_State)
	{
	case HandshakeBegin:
		if (m_Socket->bytesAvailable() >= 12)
		{
			char buf[13];
			if (!receive(buf, 12))
				return;
			m_State = HandshakeReceived;
			buf[12] = 0;
			if (buf[0] != 'R' || buf[1] != 'F' || buf[2] != 'B' || buf[3] != ' ' || buf[7] != '.')
			{
			  bad_response:
				qDebug("VNC: handshake failed (invalid response from the server).");
				disconnect();
				return;
			}
			buf[7] = 0;
			int major = 0;
			if (sscanf(&buf[4], "%3d", &major) != 1)
				goto bad_response;
			int minor = 0;
			if (sscanf(&buf[4], "%3d", &minor) != 1)
				goto bad_response;
			if (major < 3 || (major == 3 && minor < 3))
			{
				qDebug("VNC: server is too old (protocol version %d.%d).", major, minor);
				disconnect();
				return;
			}
			const char header[12] = { 'R','F','B',' ','0','0','3','.','0','0','3','\n' };
			if (!send(header, 12))
				return;
			m_State = HandshakeSent;
			goto restart;
		}

	case HandshakeReceived:
		return;

	case HandshakeSent:
		if (m_Socket->bytesAvailable() >= 4)
		{
			char buf[4];
			if (!receive(buf, 4))
				return;
			m_State = AuthRequestReceived;
			if (buf[0] != 0 || buf[1] != 0 || buf[2] != 0 || buf[3] != 2)
			{
			  bad_auth:
				qDebug("VNC: server requested unsupported authentication method.");
				disconnect();
				return;
			}
			goto restart;
		}
		return;

	case AuthRequestReceived:
		if (m_Socket->bytesAvailable() >= 4)
		{
			char buf[16];
			if (!receive(buf, 16))
				return;
			m_State = AuthDataReceived;
			if (!send(buf, 16))
				return;
			m_State = AuthResponseSent;
			goto restart;
		}
		return;

	case AuthDataReceived:
		return;

	case AuthResponseSent:
		if (m_Socket->bytesAvailable() >= 4)
		{
			char buf[4];
			if (!receive(buf, 4))
				return;
			m_State = AuthResponseReceived;
			if (buf[0] != 0 || buf[1] != 0 || buf[2] != 0 || buf[3] != 0)
			{
				qDebug("VNC: server authentication failed.");
				disconnect();
				return;
			}
			buf[0] = 1;
			if (!send(buf, 1))
				return;
			m_State = ClientInitSent;
			goto restart;
		}
		return;

	case AuthResponseReceived:
		return;

	case ClientInitSent:
		if (m_Socket->bytesAvailable() >= 24)
		{
			char buf[24];
			if (!receive(buf, 24))
				return;
			m_State = PixelFormatReceived;
			m_Width = (((int)(unsigned char)buf[0]) << 8) | (unsigned char)buf[1];
			m_Height = (((int)(unsigned char)buf[2]) << 8) | (unsigned char)buf[3];
			int bitsPerPixel = (unsigned char)buf[4];
			int depth = (unsigned char)buf[5];
			int bigEndianFlag = (unsigned char)buf[6];
			int trueColourFlag = (unsigned char)buf[7];
			int redMax = (((int)(unsigned char)buf[8]) << 8) | (unsigned char)buf[9];
			int greenMax = (((int)(unsigned char)buf[10]) << 8) | (unsigned char)buf[11];
			int blueMax = (((int)(unsigned char)buf[12]) << 8) | (unsigned char)buf[13];
			int redShift = (unsigned char)buf[14];
			int greenShift = (unsigned char)buf[15];
			int blueShift = (unsigned char)buf[16];
			m_NameLength = (((int)(unsigned char)buf[20]) << 24) |
						   (((int)(unsigned char)buf[21]) << 16) |
						   (((int)(unsigned char)buf[22]) << 8) |
						   (unsigned char)buf[23];
			qDebug("VNC: received pixel format request from the server (%dx%d, "
				"bitsPerPixel=%d, depth=%d, bigEndian=%d, trueColour=%d, redMax=%d, greenMax=%d, blueMax=%d, "
				"redShift=%d, greenShift=%d, blueShift=%d).", m_Width, m_Height, bitsPerPixel, depth,
				bigEndianFlag, trueColourFlag, redMax, greenMax, blueMax, redShift, greenShift, blueShift);
			m_State = ReceivingDesktopName;
		}
		return;

	case PixelFormatReceived:
		return;

	case ReceivingDesktopName:
		{
		qint64 numBytes = m_Socket->bytesAvailable();
		if (numBytes >= 1)
		{
			static char buf[256];
			int len = (m_NameLength > (int)sizeof(buf) ? (int)sizeof(buf) : m_NameLength);
			if ((qint64)len > numBytes)
				len = (int)numBytes;
			if (!receive(buf, len))
				return;
			m_NameLength -= len;
			if (m_NameLength <= 0)
				m_State = ReceivedDesktopName;
			goto restart;
		}
		}
		return;

	#define VNC_BITS_PER_PIXEL 32
	#define VNC_DEPTH 32
	#define VNC_BIG_ENDIAN 0
	#define VNC_TRUE_COLOUR 1
	#define VNC_RED_MAX 255
	#define VNC_GREEN_MAX 255
	#define VNC_BLUE_MAX 255
	#define VNC_RED_SHIFT 16
	#define VNC_GREEN_SHIFT 8
	#define VNC_BLUE_SHIFT 0

	case ReceivedDesktopName: {
		char buf[20];
		buf[0] = 0; /* SetPixelFormat */
		buf[1] = 0;
		buf[2] = 0;
		buf[3] = 0;
		buf[4] = VNC_BITS_PER_PIXEL;
		buf[5] = VNC_DEPTH;
		buf[6] = VNC_BIG_ENDIAN;
		buf[7] = VNC_TRUE_COLOUR;
		buf[8] = 0;
		buf[9] = (char)VNC_RED_MAX;
		buf[10] = 0;
		buf[11] = (char)VNC_GREEN_MAX;
		buf[12] = 0;
		buf[13] = (char)VNC_BLUE_MAX;
		buf[14] = VNC_RED_SHIFT;
		buf[15] = VNC_GREEN_SHIFT;
		buf[16] = VNC_BLUE_SHIFT;
		buf[17] = 0;
		buf[18] = 0;
		buf[19] = 0;
		if (!send(buf, 20))
			return;
		if (!sendFramebufferUpdateRequest())
			return;
		m_State = Ready;
		return;
		}

	case Ready:
		if (m_Socket->bytesAvailable() >= 1)
		{
			char messageType;
			if (!receive(&messageType, 1))
				return;
			switch (messageType)
			{
			case 0: m_State = FramebufferUpdateReceived; goto restart;
			case 2: m_State = Ready; /* Bell */ goto restart;
			default: qDebug("VNC: received invalid message type %d.", (int)(unsigned char)messageType);
			}
		}
		return;

	case FramebufferUpdateReceived:
		if (m_Socket->bytesAvailable() >= 3)
		{
			char buf[3];
			if (!receive(buf, 3))
				return;
			m_NumRectangles = ((int)(unsigned char)buf[1] << 8) | (int)(unsigned char)buf[2];
			m_State = ReceivingRectangles;
			emit beginFrame(m_Width, m_Height);
			goto restart;
		}
		return;

	case ReceivingRectangles:
		if (m_NumRectangles == 0)
		{
			emit endFrame();
			m_State = Ready;
			if (!sendFramebufferUpdateRequest())
				return;
			goto restart;
		}
		if (m_Socket->bytesAvailable() >= 12)
		{
			char buf[12];
			if (!receive(buf, 12))
				return;
			--m_NumRectangles;
			m_RectX = ((int)(unsigned char)buf[0] << 8) | (int)(unsigned char)buf[1];
			m_RectY = ((int)(unsigned char)buf[2] << 8) | (int)(unsigned char)buf[3];
			m_RectW = ((int)(unsigned char)buf[4] << 8) | (int)(unsigned char)buf[5];
			m_RectH = ((int)(unsigned char)buf[6] << 8) | (int)(unsigned char)buf[7];
			m_RectDataLength = m_RectW * m_RectH * 4;
			m_RectDataOffset = 0;
			m_RectData.resize(m_RectDataLength);
			m_Encoding = ((int)(unsigned char)buf[8] << 24) |
						 ((int)(unsigned char)buf[9] << 16) |
						 ((int)(unsigned char)buf[10] << 8) |
						 (int)(unsigned char)buf[11];
			if (m_Encoding != 0)
			{
				qDebug("VNC: received invalid encoding %d.", m_Encoding);
				disconnect();
				return;
			}
			m_State = ReceivingRectangle;
		}
		return;

	case ReceivingRectangle:
		{
		qint64 numBytes = m_Socket->bytesAvailable();
		if (numBytes >= 1)
		{
			int len = m_RectDataLength - m_RectDataOffset;
			if ((qint64)len > numBytes)
				len = (int)numBytes;
			if (!receive(m_RectData.data() + m_RectDataOffset, len))
				return;
			m_RectDataOffset += len;
			if (m_RectDataOffset >= m_RectDataLength)
			{
				m_State = ReceivingRectangles;
				emit processRect(m_RectX, m_RectY, m_RectW, m_RectH, m_RectData);
			}
			goto restart;
		}
		}
		return;
	}
}

bool VNCClient::send(const void * buffer, size_t length)
{
	const char * p = (const char *)buffer;
	while (length > 0)
	{
		qint64 bytesSent;
		if (!m_Socket->isOpen())
			goto err;
		bytesSent = m_Socket->write(p, (qint64)length);
		if (bytesSent < 0)
		{
		  err:
			qDebug("VNC: unable to send data to the server.");
			disconnect();
			return false;
		}
		length -= (size_t)bytesSent;
		p += (size_t)bytesSent;
	}
	return true;
}

bool VNCClient::receive(void * buffer, size_t length)
{
	char * p = (char *)buffer;
	while (length > 0)
	{
		qint64 bytesRead;
		if (!m_Socket->isOpen())
			goto err;
		bytesRead = m_Socket->read(p, (qint64)length);
		if (bytesRead < 0)
		{
		  err:
			qDebug("VNC: unable to receive data from the server.");
			disconnect();
			return false;
		}
		length -= (size_t)bytesRead;
		p += (size_t)bytesRead;
	}
	return true;
}

bool VNCClient::sendFramebufferUpdateRequest()
{
	char buf[10];
	buf[0] = 3; /* FramebufferUpdateRequest */
	buf[1] = 0; /* incremental */
	buf[2] = 0; /* x */
	buf[3] = 0;
	buf[4] = 0; /* y */
	buf[5] = 0;
	buf[6] = (char)((m_Width >> 8) & 0xFF); /* w */
	buf[7] = (char)(m_Width & 0xFF);
	buf[8] = (char)((m_Height >> 8) & 0xFF); /* h */
	buf[9] = (char)(m_Height & 0xFF);
	return send(buf, 10);
}
