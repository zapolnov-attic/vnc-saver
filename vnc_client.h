//
//	Very simple VNC client class.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __VNC_CLIENT_H__
#define __VNC_CLIENT_H__

#include <QTcpSocket>

class VNCClient : public QObject
{
	Q_OBJECT

public:
	VNCClient(const QString & host, int port = 5900, QObject * parent = 0);

	Q_SLOT void disconnect();

	Q_SIGNAL void connectionClosed();

	Q_SIGNAL void beginFrame(int w, int h);
	Q_SIGNAL void processRect(int x, int y, int w, int h, QByteArray data);
	Q_SIGNAL void endFrame();

private:
	enum State
	{
		HandshakeBegin,
		HandshakeReceived,
		HandshakeSent,
		AuthRequestReceived,
		AuthDataReceived,
		AuthResponseSent,
		AuthResponseReceived,
		ClientInitSent,
		PixelFormatReceived,
		ReceivingDesktopName,
		ReceivedDesktopName,
		FramebufferUpdateReceived,
		Ready,
		ReceivingRectangles,
		ReceivingRectangle
	};

	QTcpSocket * m_Socket;
	State m_State;
	int m_NameLength;
	int m_NumRectangles;
	int m_Width;
	int m_Height;
	int m_RectX;
	int m_RectY;
	int m_RectW;
	int m_RectH;
	int m_Encoding;
	QByteArray m_RectData;
	int m_RectDataOffset;
	int m_RectDataLength;

	Q_SLOT void connected();
	Q_SLOT void disconnected();
	Q_SLOT void readyRead();

	bool send(const void * buffer, size_t length);
	bool receive(void * buffer, size_t length);

	bool sendFramebufferUpdateRequest();
};

#endif
