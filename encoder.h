//
//	Video stream encoder class.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __ENCODER_H__
#define __ENCODER_H__

#include <QImage>
#include <QIODevice>
#include <QObject>
#include "codec.h"

class Encoder : public QObject
{
	Q_OBJECT

public:
	Encoder(Codec * codec, QIODevice * output, QObject * parent = 0);
	~Encoder();

	Q_SLOT void beginFrame(int w, int h);
	Q_SLOT void processRect(int x, int y, int w, int h, QByteArray data);
	Q_SLOT void endFrame();

private:
	Codec * m_Codec;
	QIODevice * m_Output;
	QImage * m_Image;
	bool m_InFrame;
};

#endif
