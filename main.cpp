//
//	VNC client that encodes everything it receives into a video file.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include <QFile>
#include <QApplication>
#include "encoder.h"
#include "lagarith_codec.h"
#include "vnc_client.h"

int main(int argc, char ** argv)
{
	QApplication app(argc, argv);

	QFile file("output.avi");
	if (!file.open(QFile::WriteOnly))
		return 1;

	LagarithCodec codec;
	Encoder encoder(&codec, &file);
	VNCClient client("127.0.0.1");

	QObject::connect(&client, SIGNAL(beginFrame(int,int)), &encoder, SLOT(beginFrame(int,int)));
	QObject::connect(&client, SIGNAL(processRect(int,int,int,int,QByteArray)),
		&encoder, SLOT(processRect(int,int,int,int,QByteArray)));
	QObject::connect(&client, SIGNAL(endFrame()), &encoder, SLOT(endFrame()));
	QObject::connect(&client, SIGNAL(connectionClosed()), &app, SLOT(quit()));

	return app.exec();
}
