//
//	Wrapper for the Lagarith codec.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __LAGARITH_CODEC_H__
#define __LAGARITH_CODEC_H__

#include "codec.h"
#include "3rdparty/lagarith/windows/vfw.h"

class LagarithCodec : public Codec
{
	Q_OBJECT

public:
	LagarithCodec(QObject * parent = 0);
	~LagarithCodec();

	bool init(int width, int height);
	bool end();

	QByteArray compressFrame(const void * frameData);

private:
	DWORD_PTR m_CodecHandle;
	int m_BufferSize;
	BITMAPINFOHEADER m_InputFormat;
	LPBITMAPINFOHEADER m_OutputFormat;
	QByteArray m_Buffer;
	QByteArray m_PrevFrameData;
	LONG m_FrameNum;
	int m_FrameDataSize;
};

#endif
