//
//	Replacement file for Windows(tm) header file to build lagaryth on non-Windows OSes.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __WINDOWS_H__
#define __WINDOWS_H__

#include <string.h>
#include <stddef.h>

#ifdef _MSC_VER
 typedef unsigned __int8 BYTE;
 typedef unsigned __int16 WCHAR;
 typedef unsigned __int16 WORD;
 typedef unsigned __int32 DWORD;
 typedef unsigned __int32 UINT;
 typedef unsigned __int32 UINT32;
 typedef __int32 BOOL;
 typedef __int32 LONG;
#else
 #include <stdint.h>
 #define __int64 long long
 typedef uint8_t BYTE;
 typedef uint16_t WCHAR;
 typedef uint16_t WORD;
 typedef uint32_t DWORD;
 typedef uint32_t UINT;
 typedef uint32_t UINT32;
 typedef int32_t BOOL;
 typedef int32_t LONG;
#endif

typedef ptrdiff_t LONG_PTR;
typedef size_t UINT_PTR;
typedef size_t ULONG_PTR;

typedef ULONG_PTR DWORD_PTR;
typedef LONG_PTR LRESULT;
typedef LONG_PTR LPRESULT;
typedef UINT_PTR WPARAM;
typedef LONG_PTR LPARAM;

typedef void * LPVOID;
typedef void * HANDLE;
typedef void * HWND;
typedef void * HDRVR;

typedef DWORD * LPDWORD;

#define PASCAL

typedef struct tagBITMAPINFOHEADER
{
	DWORD biSize;
	LONG biWidth;
	LONG biHeight;
	WORD biPlanes;
	WORD biBitCount;
	DWORD biCompression;
	DWORD biSizeImage;
	LONG biXPelsPerMeter;
	LONG biYPelsPerMeter;
	DWORD biClrUsed;
	DWORD biClrImportant;
}
BITMAPINFOHEADER, * PBITMAPINFOHEADER, * LPBITMAPINFOHEADER;

#define INFINITE ((DWORD)0xFFFFFFFFUL)
#define WAIT_OBJECT_0 0

#ifdef _WIN32
 #include <float.h>
 #pragma fenv_access (on)
#else
 #include <fenv.h>
 #pragma STDC FENV_ACCESS ON
 #define _MCW_RC 0x00000300
 #define _MCW_PC 0x00030000
 #define _PC_53 0x00010000
 #define _RC_NEAR 0x00000000
 static
 #ifdef __cplusplus
 inline
 #endif
 unsigned int _controlfp(unsigned int newf, unsigned int mask)
 {
	unsigned short cw = 0;
  #if (defined(__GNUC__) || defined(__clang__)) && \
		(defined(i386) || defined(__i386) || defined (__i386__) || defined(_M_IX86) || defined(_X86_))
	#define _FPU_GETCW(cw) __asm__ __volatile__ ("fnstcw %0" : "=m" (*&cw))
	#define _FPU_SETCW(cw) __asm__ __volatile__ ("fldcw %0" : : "m" (*&cw))
  #else
	#define _FPU_GETCW(cw) (void)(cw)
	#define _FPU_SETCW(cw) (void)(cw)
  #endif
	_FPU_GETCW(cw);
	cw = (unsigned short)((cw & ~mask) | (newf & mask));
	_FPU_SETCW(cw);
	return cw;
 }
#endif

#define TRUE 1

#define IsBadWritePtr(a,b) (0)

#endif
