//
//	Replacement file for Windows(tm) header file to build lagaryth on non-Windows OSes.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "malloc.h"
#include <stdlib.h>

#ifndef _WIN32

void * _aligned_malloc(size_t size, size_t alignment)
{
	char * ptr = (char *)malloc(sizeof(void *) + (alignment - 1) + size);
	if (!ptr)
		return NULL;
	size_t off = (size_t)(ptr + sizeof(void *));
	off = (off + alignment - 1) & ~(alignment - 1);
	char * nptr = (char *)off;
	void ** p = (void **)nptr - 1;
	*p = ptr;
	return nptr;
}

void _aligned_free(void * ptr)
{
	if (!ptr)
		return;
	void ** p = (void **)ptr - 1;
	free(*p);
}

#else
extern const char _dummy_malloc_cpp_ = '.';
#endif
