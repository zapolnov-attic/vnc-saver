//
//	Replacement file for Windows(tm) header file to build lagaryth on non-Windows OSes.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __MALLOC_H__
#define __MALLOC_H__

#include <stddef.h>

#ifdef _WIN32
 #include <malloc.h>
#else
 void * _aligned_malloc(size_t size, size_t alignment);
 void _aligned_free(void * ptr);
#endif

#endif
