//
//	Replacement file for Windows(tm) header file to build lagaryth on non-Windows OSes.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __VFW_H__
#define __VFW_H__

#include "../windows/windows.h"

#define ICVERSION 0x104

#define mmioFOURCC(ch0, ch1, ch2, ch3) MAKEFOURCC(ch0, ch1, ch2, ch3)

#define MAKEFOURCC(ch0, ch1, ch2, ch3)  \
	((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |  \
	((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24))

#define ICTYPE_VIDEO mmioFOURCC('v','i','d','c')

#define ICERR_OK 0
#define ICERR_UNSUPPORTED -1
#define ICERR_BADFORMAT -2
#define ICERR_MEMORY -3
#define ICERR_BADSIZE -7
#define ICERR_ERROR -100

#define VIDCF_FASTTEMPORALC 0x0020
#define VIDCF_FASTTEMPORALD 0x0080

#define DRV_LOAD 0x0001
#define DRV_ENABLE 0x0002
#define DRV_OPEN 0x0003
#define DRV_CLOSE 0x0004
#define DRV_DISABLE 0x0005
#define DRV_FREE 0x0006
#define DRV_CONFIGURE 0x0007
#define DRV_QUERYCONFIGURE 0x0008
#define DRV_INSTALL 0x0009
#define DRV_REMOVE 0x000A
#define DRV_USER 0x4000

#define ICM_USER (DRV_USER + 0x0000)
#define ICM_RESERVED_LOW (DRV_USER + 0x1000)
#define ICM_RESERVED_HIGH (DRV_USER + 0x2000)
#define ICM_RESERVED ICM_RESERVED_LOW
#define ICM_GETSTATE (ICM_RESERVED + 0)
#define ICM_SETSTATE (ICM_RESERVED + 1)
#define ICM_GETINFO (ICM_RESERVED + 2)
#define ICM_CONFIGURE (ICM_RESERVED + 10)
#define ICM_ABOUT (ICM_RESERVED + 11)
#define ICM_GETDEFAULTQUALITY (ICM_RESERVED + 30)
#define ICM_COMPRESS_GET_FORMAT (ICM_USER + 4)
#define ICM_COMPRESS_GET_SIZE (ICM_USER + 5)
#define ICM_COMPRESS_QUERY (ICM_USER + 6)
#define ICM_COMPRESS_BEGIN (ICM_USER + 7)
#define ICM_COMPRESS (ICM_USER + 8)
#define ICM_COMPRESS_END (ICM_USER + 9)
#define ICM_DECOMPRESS_GET_FORMAT (ICM_USER + 10)
#define ICM_DECOMPRESS_QUERY (ICM_USER + 11)
#define ICM_DECOMPRESS_BEGIN (ICM_USER + 12)
#define ICM_DECOMPRESS (ICM_USER + 13)
#define ICM_DECOMPRESS_END (ICM_USER + 14)
#define ICM_DECOMPRESS_GET_PALETTE (ICM_USER + 30)

#define AVIIF_KEYFRAME 0x00000010

#define DRVCNF_OK 0x0001
#define DRV_OK DRVCNF_OK

typedef struct
{
	DWORD dwSize;
	DWORD fccType;
	DWORD fccHandler;
	DWORD dwVersion;
	DWORD dwFlags;
	LPRESULT dwError;
	LPVOID pV1Reserved;
	LPVOID pV2Reserved;
	DWORD dnDevNode;
}
ICOPEN;

typedef struct
{
	DWORD dwSize;
	DWORD fccType;
	DWORD fccHandler;
	DWORD dwFlags;
	DWORD dwVersion;
	DWORD dwVersionICM;
	WCHAR szName[16];
	WCHAR szDescription[128];
	WCHAR szDriver[128];
}
ICINFO;

typedef struct
{
	DWORD dwFlags;
	LPBITMAPINFOHEADER lpbiOutput;
	LPVOID lpOutput;
	LPBITMAPINFOHEADER lpbiInput;
	LPVOID lpInput;
	LPDWORD lpckid;
	LPDWORD lpdwFlags;
	LONG lFrameNum;
	DWORD dwFrameSize;
	DWORD dwQuality;
	LPBITMAPINFOHEADER lpbiPrev;
	LPVOID lpPrev;
}
ICCOMPRESS;

typedef struct
{
	DWORD dwFlags;
	LPBITMAPINFOHEADER lpbiInput;
	LPVOID lpInput;
	LPBITMAPINFOHEADER lpbiOutput;
	LPVOID lpOutput;
	DWORD ckid;
}
ICDECOMPRESS;

#define DefDriverProc(dwDriverID, hDriver, uiMessage, lParam1, lParam2) (ICERR_UNSUPPORTED)

#ifdef __cplusplus
extern "C" {
#endif

LRESULT PASCAL DriverProc(DWORD_PTR dwDriverID, HDRVR hDriver, UINT uiMessage, LPARAM lParam1, LPARAM lParam2);

#ifdef __cplusplus
}
#endif

#endif
