//
//	Replacement file for Windows(tm) header file to build lagaryth on non-Windows OSes.
//	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __INTRIN_H__
#define __INTRIN_H__

#ifdef _MSC_VER
 #include <intrin.h>
#else
 #define __cpuid(a,b) ((void)((a)[0]=0,(a)[1]=0,(a)[2]=0,(a)[3]=0))
 #define __emulu(a,b) ((unsigned long long)((unsigned long long)(a) * (unsigned long long)(b)))
 #define __assume(x) ((void)0)
#endif

#endif
