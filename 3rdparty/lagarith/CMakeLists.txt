#
#	CMakeLists.txt for building lagarith.
#	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation; either version 2
#	of the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

ADD_DEFINITIONS(-DX64_BUILD)

IF(CMAKE_COMPILER_IS_GNUCC OR CMAKE_CXX_COMPILER_ID MATCHES ".*Clang.*")
	ADD_DEFINITIONS(-Wno-unknown-pragmas -Wno-multichar)
ENDIF()

ADD_LIBRARY(lagarith STATIC
	windows/intrin.h
	windows/malloc.cpp
	windows/malloc.h
	windows/vfw.h
	windows/windows.h
	Fibonacci.h
	compact.cpp
	compact.h
	compact_old.cpp
	compression.cpp
	convert.h
	convert_lagarith.cpp
	convert_lagarith.h
	convert_yv12.h
	decompression.cpp
	drvproc.cpp
	fibonacci.cpp
	interface.cpp
	interface.h
	lagarith.h
	prediction.cpp
	prediction.h
	range.cpp
	reciprocal_table.cpp
	resource.h
	threading.h
	zerorle.cpp
	zerorle.h
)

SET_SOURCE_FILES_PROPERTIES(reciprocal_table.cpp PROPERTIES HEADER_FILE_ONLY TRUE)
