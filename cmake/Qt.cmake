#
#	CMake script to detect presence and version of the Qt library (both Qt4 and Qt5 are supported).
#	Copyright (c) 2013 Nikolay Zapolnov (zapolnov@gmail.com).
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation; either version 2
#	of the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

SET(QT_FOUND FALSE)

############################################################
## Try to determine Qt version and installation directory ##
############################################################

SET(QT_ERROR FALSE)
IF(NOT QT_INSTALL_DIRECTORY OR NOT QT_LIBS_DIRECTORY)

	FIND_PROGRAM(QMAKE_EXECUTABLE_PATH qmake HINTS ENV QTDIR DOC "Path to the qmake executable")
	IF(NOT QMAKE_EXECUTABLE_PATH)
		MESSAGE(WARNING "Executable 'qmake' was not found in path.")
		SET(QT_ERROR TRUE)
	ENDIF()

	##############################################################################################################
	## Get path to the Qt

	IF(NOT QT_ERROR AND NOT QT_INSTALL_DIRECTORY)

		EXECUTE_PROCESS(COMMAND
			"${QMAKE_EXECUTABLE_PATH}" -query QT_INSTALL_PREFIX
			RESULT_VARIABLE QMAKE_EXEC_RESULT
			OUTPUT_VARIABLE QT_INSTALL_DIRECTORY
			OUTPUT_STRIP_TRAILING_WHITESPACE
		)

		IF(NOT "${QMAKE_EXEC_RESULT}" STREQUAL "0")
			MESSAGE(WARNING "Unable to execute 'qmake -query QT_INSTALL_PREFIX': ${QMAKE_EXEC_RESULT}")
			SET(QT_ERROR TRUE)
		ENDIF()

	ENDIF()

	##############################################################################################################
	## Get path to the Qt 'libs' directory

	IF(NOT QT_ERROR AND NOT QT_LIBS_DIRECTORY)

		EXECUTE_PROCESS(COMMAND
			"${QMAKE_EXECUTABLE_PATH}" -query QT_INSTALL_LIBS
			RESULT_VARIABLE QMAKE_EXEC_RESULT
			OUTPUT_VARIABLE QT_LIBS_DIRECTORY
			OUTPUT_STRIP_TRAILING_WHITESPACE
		)

		IF(NOT "${QMAKE_EXEC_RESULT}" STREQUAL "0")
			MESSAGE(WARNING "Unable to execute 'qmake -query QT_INSTALL_LIBS': ${QMAKE_EXEC_RESULT}")
			SET(QT_ERROR TRUE)
		ENDIF()

	ENDIF()

	##############################################################################################################
	## Get Qt version

	EXECUTE_PROCESS(COMMAND
		"${QMAKE_EXECUTABLE_PATH}" -query QT_VERSION
		RESULT_VARIABLE QMAKE_EXEC_RESULT
		OUTPUT_VARIABLE QT_VERSION
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)

	IF(NOT "${QMAKE_EXEC_RESULT}" STREQUAL "0")
		MESSAGE(WARNING "Unable to execute 'qmake -query QT_VERSION': ${QMAKE_EXEC_RESULT}")
		SET(QT_ERROR TRUE)
	ENDIF()

	##############################################################################################################
	## Parse the Qt version number

	IF(NOT QT_ERROR)
		SET(QT_MAJOR 0)
		STRING(REGEX REPLACE "^([0-9]+)\\.[0-9]+\\.[0-9]+" "\\1" QT_MAJOR "${QT_VERSION}")
		IF(QT_MAJOR EQUAL 4)
			SET(IS_QT4 TRUE CACHE INTERNAL "Is Qt4" FORCE)
		ELSE(IF QT_MAJOR GREATER 4)
			SET(IS_QT4 FALSE CACHE INTERNAL "Is Qt4" FORCE)
		ELSE()
			MESSAGE(WARNING "Unable to parse Qt version string (\"${QT_VERSION}\").")
			SET(QT_ERROR TRUE)
		ENDIF()
	ENDIF()

	##############################################################################################################
	## Report that we have found a Qt library

	IF(NOT QT_ERROR)
		MESSAGE(STATUS "Found Qt ${QT_VERSION}: ${QT_INSTALL_DIRECTORY}")
		SET(QT_INSTALL_DIRECTORY "${QT_INSTALL_DIRECTORY}" CACHE PATH "Path to the Qt SDK" FORCE)
		SET(QT_LIBS_DIRECTORY "${QT_LIBS_DIRECTORY}" CACHE PATH "Path to the Qt SDK libraries" FORCE)
	ENDIF()

ENDIF()

#######################
## Search for the Qt ##
#######################

SET(QT_FOUND FALSE)
IF(NOT QT_ERROR)

	SET(CMAKE_PREFIX_PATH "${QT_INSTALL_DIRECTORY}" ${CMAKE_PREFIX_PATH})

	IF(NOT IS_QT4)
		FIND_PACKAGE(Qt5Widgets)
		IF(Qt5Widgets_FOUND)
		FIND_PACKAGE(Qt5Network REQUIRED)
			FIND_PACKAGE(Qt5OpenGL REQUIRED)
			SET(QT_FOUND TRUE)
			SET(QT_LIBRARIES ${Qt5Widgets_LIBRARIES} ${Qt5Network_LIBRARIES} ${Qt5OpenGL_LIBRARIES})
		ENDIF()
	ENDIF()

	IF(NOT QT_FOUND)
		FIND_PACKAGE(Qt4 4.8.1 COMPONENTS QtCore QtGui QtNetwork QtOpenGL)
		IF(QT4_FOUND)
			SET(QT_FOUND TRUE)
			SET(IS_QT4 TRUE CACHE INTERNAL "Is Qt4" FORCE)
		ENDIF()
	ENDIF()

	IF(NOT QT_FOUND)
		SET(QT_LIBRARIES)
	ENDIF()

ENDIF()

###############################################
## Configure build options and macros for Qt ##
###############################################

IF(QT_FOUND AND NOT QT_ERROR)
	LINK_DIRECTORIES("${QT_LIBS_DIRECTORY}")
ENDIF()

IF(QT_FOUND)
	IF(Qt5Widgets_FOUND)
		INCLUDE_DIRECTORIES(${Qt5Widgets_INCLUDES} ${Qt5Network_INCLUDES} ${Qt5OpenGL_INCLUDES})
		INCLUDE_DIRECTORIES(${Qt5Widgets_INCLUDE_DIRS} ${Qt5Network_INCLUDE_DIRS} ${Qt5OpenGL_INCLUDE_DIRS})
		ADD_DEFINITIONS(${Qt5Widgets_DEFINITIONS} ${Qt5Network_DEFINITIONS} ${Qt5OpenGL_DEFINITIONS})
		SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")
		SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Network_EXECUTABLE_COMPILE_FLAGS}")
		SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5OpenGL_EXECUTABLE_COMPILE_FLAGS}")
		MACRO(QT_ADD_RESOURCES var)
			QT5_ADD_RESOURCES(${var} ${ARGN})
		ENDMACRO()
	ELSE()
		INCLUDE(${QT_USE_FILE})
		MACRO(QT_ADD_RESOURCES var)
			QT4_ADD_RESOURCES(${var} ${ARGN})
		ENDMACRO()
	ENDIF()
ENDIF()
